﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IRepositoryProvider
    {
        IRepository<TEntity> GetStandardRepository<TEntity>() where TEntity : class;
        TRepositoryInterface GetCustomRepository<TRepositoryInterface>();
    }
}
