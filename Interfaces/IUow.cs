﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IUow
    {
        IRepository<Event> Events { get; }
        IRepository<UserData> UserData { get; }
        IRepository<UserInEvent> UserInEvent { get; }
        IRepository<EventComment> EventComment { get; }
        IRepository<UserDataType> UserDataType { get; }
        IRepository<Record> Record { get; }

        IUserRepository Users { get; }
        IRepository<EventCategory> EventCategory { get; }
        IRepository<Category> Category { get; }
        int SaveChanges();
    }
}
