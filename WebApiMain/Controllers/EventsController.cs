﻿using BL.DTOs;
using BL.Services.EventService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security.Claims;
using Microsoft.AspNet.Identity;

namespace WebApiMain.Controllers
{
    [RoutePrefix("api/Events")]
    public class EventsController : ApiController
    {
        private readonly IEventService _eventService;

        public EventsController(IEventService eventService)
        {
            _eventService = eventService;
        }

        [HttpGet]
        public IHttpActionResult Get()
        {
            var events = _eventService.GetList();
            if (events == null)
            {
                return Content(HttpStatusCode.NotFound, "Can not find event list.");
            }
            return Ok(events);
        }

        [HttpPost]
        [Route("GetEvents")]
        public IHttpActionResult GetEvents([FromBody] FilterDTO filterDTO)
        {
            var events = _eventService.getFilteredEvents(filterDTO);

            if (events == null)
            {
                return Content(HttpStatusCode.NotFound, "Can not find event list.");
            }
            return Ok(events);
        }

        [HttpGet]
        [Authorize]
        [Route("{eventId}/Participate")]
        public IHttpActionResult Participate(int eventId)
        {
            string participantId = User.Identity.GetUserId();
            var status = _eventService.Participate(eventId, participantId);
            if (!status)
            {
                return Content(HttpStatusCode.NotAcceptable, "Could not participate in event.");
            }
            return Ok("Participating in event");
        }

        [HttpGet]
        [Route("{eventId}/ParticipantsCount")]
        public IHttpActionResult ParticipantsCount(int eventId)
        {
            var count = _eventService.ParticipantCount(eventId);

            if (count == -1)
            {
                return Content(HttpStatusCode.NotFound, "Event not found");
            }
            return Ok(count);
        }

        [HttpGet]
        [Route("{eventId}/Participants")]
        public IHttpActionResult GetParticipants(int eventId)
        {
            var participants = _eventService.GetParticipants(eventId);

            if (participants == null)
            {
                return Content(HttpStatusCode.NotFound, "Event not found");
            }
            return Ok(participants);
        }

        [HttpGet]
        [Route("categories")]
        public IHttpActionResult GetCategories()
        {
            var categories = _eventService.GetAllCategories();

            if (categories != null)
            {
                return Ok(categories);
            } else
            {
                return Content(HttpStatusCode.NotFound, "Can not find categories");
            }
        }

        [HttpGet]
        [Route("{eventId}/Comments")]
        public IHttpActionResult GetComments(int eventId)
        {
            var comments = _eventService.GetComments(eventId);
            if (!_eventService.eventExists(eventId))
            {
                return Content(HttpStatusCode.NotFound, "Event not found");
            }
            return Ok(comments);
        }

        [Authorize]
        [HttpPost]
        [Route("{eventId}/Comments")]
        public IHttpActionResult PostComment([FromBody] CommentDTO comment)
        {
            string participantId = User.Identity.GetUserId();

            if (!_eventService.eventExists(comment.EventId))
            {
                return Content(HttpStatusCode.NotFound, "Event not found");
            }
            var cmnt = _eventService.PostComment(comment, participantId);

            if (cmnt != null)
            {
                return Ok(cmnt);

            } 
            return Content(HttpStatusCode.BadRequest, "Comment not added!");
        }


        [HttpGet]
        public IHttpActionResult GetById(int id)
        {

            if (!_eventService.eventExists(id))
            {
                return Content(HttpStatusCode.NotFound, "Event not found");
            }
            var evnt = _eventService.GetEvent(id);

            return Ok(evnt);
        }

        // POST api/<controller>
        [Authorize]
        [HttpPost]
        public IHttpActionResult Post([FromBody] EventDTO eventDTO)
        {
            string userId = User.Identity.GetUserId();
            var categoryIds = eventDTO.CategoryIds;
            var evnt = _eventService.AddEvent(eventDTO, userId, categoryIds);

            if (evnt == null)
            {
                return Content(HttpStatusCode.BadRequest, "Can not post event with id: " + evnt.EventId);
            }
            return Ok(evnt);
        }

        [Authorize]
        [HttpGet]
        [Route("{eventId}/isparticipant")]
        public IHttpActionResult isParticipant(int eventId)
        {
            string participantId = User.Identity.GetUserId();
            if (!_eventService.eventExists(eventId)) {
                return Content(HttpStatusCode.NotFound, "Event not found");
            }
            return Ok(_eventService.isParticipant(eventId, participantId));
        }
    }
}