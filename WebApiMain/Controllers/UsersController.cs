﻿using BL.DTOs;
using BL.Services.UserService;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;


namespace WebApiMain.Controllers
{
    public class UsersController : ApiController
    {

        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [Authorize]
        [HttpGet]
        [Route("api/Users/LoggedInUser")]
        public IHttpActionResult GetUser()
        {
            string id = User.Identity.GetUserId();
            if (!_userService.userExists(id))
            {
                return Content(HttpStatusCode.NotFound, "User not found!");
            }
            var user = _userService.GetUserWithData(id);
            return Ok(user);
        }

        [HttpGet]
        [Route("api/Users/OrganizerUser/{userId}")]
        public IHttpActionResult GetOrganizerUser(string userId)
        {
            if (!_userService.userExists(userId))
            {
                return Content(HttpStatusCode.NotFound, "Organizer not found!");
            }
            var organizer = _userService.GetUserWithData(userId);
            return Ok(_userService.GetUserWithData(userId));
        }

        [Authorize]
        [HttpPut]
        [Route("api/Users/EditUser/{userId}")]
        public IHttpActionResult EditUser(int userId, [FromBody] UserDTO dto)
        {
            _userService.EditUserData(userId, dto);
            return Ok();
        }

        [Authorize]
        [HttpGet]
        [Route("api/Users/Events")]
        public IHttpActionResult GetUserEvents()
        {
            string userAspNetId = User.Identity.GetUserId();
            if (!_userService.userExists(userAspNetId))
            {
                return Content(HttpStatusCode.NotFound, "User not found!");
            }
            var userEvents = _userService.GetUserEvents(userAspNetId);
            return Ok(userEvents);
        }
    }
}
