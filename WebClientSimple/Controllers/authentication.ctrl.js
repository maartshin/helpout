﻿(function () {
    'use strict';

    angular.module('app').controller('AuthenticationCtrl', Ctrl);
    console.log("sign up ctrl");

    function Ctrl($http, $location) {
        var vm = this;

        this.user = {};

        this.submitSignUp = submitSignUp;
        this.login = login;

        if (sessionStorage.getItem('accessToken')) {
            console.log("Token Yes");
        } else {
            console.log("Token No");
        }

        function submitSignUp() {
            $http.post('http://localhost:56265/api/Account/Register', this.user).then(function () {
                console.log('User added');
                $location.path('/login');
            }).catch(error => console.log(error))
        }

        function login() {
            console.log("username:" + vm.user.username);
            console.log("password:" + vm.user.password);
            $http({
                method: 'POST',
                url: 'http://localhost:56265/Token',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: 'grant_type=password&username=' + vm.user.username + '&password=' + vm.user.password
            }).then(function (response) {
                    sessionStorage.setItem('accessToken', response.data.access_token);
                    console.log(response.data.access_token);
                    //sessionStorage.setItem('loggedInUserUsername', response.data.userName);
                    console.log(response.data.userName);
                    $location.path('/event/list');
                }).catch(error => console.log(error));
            
            
           
        }
    }
})();