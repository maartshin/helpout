﻿(function () {
    'use strict';

    angular.module('app').controller('EventsCtrl', Ctrl);

    function Ctrl($http, $routeParams, $location) {
        var vm = this;
        var url = 'http://localhost:56265/';

        this.event = {};
        this.events = [];
        this.comments = [];

        this.createEvent = createEvent;
        this.getEvents = getEvents;
        this.getEvent = getEvent;
       

        function createEvent() {
            if (!sessionStorage.getItem('accessToken')) {
                $location.path('/login');
                console.log("Create event denied: User not logged in.")
                return;
            }

            //this.event.ParticipantsId = sessionStorage.getItem('loggedInUserId');
            //console.log('Organizer id: ' + this.event.ParticipantsId);
            $http({
                method: 'POST',
                url: url + 'api/events',
                headers: {Authorization: 'Bearer '+sessionStorage.getItem('accessToken')},
                data: this.event
            }).then(function (result) { 
                $location.path('event/view/' + result.data.eventId);
            }).catch(error => console.log(error))
        }

        function getEvents(id) {
            if ($routeParams.id)
                return;
            $http.get(url+'api/events').then(result => vm.events = result.data);
        }

        function getEvent(id) {
            $location.path('event/view/' + id);
        }

        function getComments() {
            if (!$routeParams.id)
                return;
            $http.get(url+'api/comments/' + $routeParams.id).then(result => vm.comments = result.data);
        }
    }
})();