﻿(function () {
    'use strict';

    angular.module('app').controller('EventCtrl', Ctrl);

    function Ctrl($http, $routeParams, $location) {
        var url = 'http://localhost:56265/';
        var vm = this;

        this.event = {};
        this.organizer = {};

        initEventPreview();
        //getOrganizer();

        function initEventPreview() {
            console.log('Event id: ' + $routeParams.id);
            $http.get(url + 'api/events/' + $routeParams.id)
                .then(result => {
                    vm.event = result.data;
                }).then(getOrganizer);
        }

        function getOrganizer() {
            $http.get(url+'api/events/'+vm.event.eventId+'/organizer')
                .then(result => {
                    vm.organizer = result.data;
                    console.log(result);
                });
        }
       
    }
})();