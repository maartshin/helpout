﻿(function () {
    'use strict';

    angular.module('app').config(RouteConfig);

    function RouteConfig($routeProvider) {
        $routeProvider.when('/signup', {
            templateUrl: '/Views/signUp.html',
            controller: 'AuthenticationCtrl',
            controllerAs: 'vm'
        }).when('/login', {
            templateUrl: '/Views/login.html',
            controller: 'AuthenticationCtrl',
            controllerAs: 'vm'
        }).when('/event/list', {
            templateUrl: '/Views/Events/List.html',
            controller: 'EventsCtrl',
            controllerAs: 'vm'
        }).when('/event/new', {
            templateUrl: '/Views/Events/Edit.html',
            controller: 'EventsCtrl',
            controllerAs: 'vm'
        }).when('/event/view/:id', {
            templateUrl: '/Views/Events/View.html',
            controller: 'EventCtrl',
            controllerAs: 'vm'
        }).when('/profile', {
            templateUrl: '/Views/profile.html',
            controller: 'EventsCtrl',
            controllerAs: 'vm'
        }).otherwise('/event/list');
    }

})();