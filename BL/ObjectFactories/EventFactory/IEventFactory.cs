﻿using BL.DTOs;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.ObjectFactories.EventFactory
{
    public interface IEventFactory
    {
        EventDTO Create(Event evnt);
        SingleEventDTO CreateForSingle(Event evnt);
        CommentDTO CreateComment(EventComment eventComment);
        EventComment CreateComment(CommentDTO dto);
    }
}
