﻿using BL.DTOs;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.ObjectFactories.EventFactory
{
    public class EventFactory : IEventFactory
    {

        public EventDTO Create(Event evnt)
        {
            return new EventDTO()
            {
                EventId = evnt.EventId,
                Name = evnt.Name,
                Location = evnt.Location,
                Categories = evnt.EventCategory
                .Where(x => x.EventId == evnt.EventId)
                .Select(x => CategoryFactory.CategoryFactory.Create(x.Category)).ToList(),
                VolunteersLimit = evnt.VolunteersLimit,
                Priority = evnt.Priority,
                Description = evnt.Description,
                Created = DateTime.Now,
                StartTime = evnt.Start,
                ImageUrl = evnt.ImageUrl,
                EndTime = evnt.End

            };
        }

        public Event Create(EventDTO dto)
        {
            return new Event()
            {
                EventId = dto.EventId,
                Name = dto.Name,
                VolunteersLimit = dto.VolunteersLimit,
                Priority = dto.Priority,
                Location = dto.Location,
                Description = dto.Description,
                Created = DateTime.Now,
                Start = dto.StartTime,
                End = dto.EndTime,
                ImageUrl = dto.ImageUrl
            };
        }

        public EventComment CreateComment(CommentDTO dto)
        {
            var comment = new EventComment()
            {
                UserId = dto.UserId,
                CommentText = dto.CommentText,
                Since = DateTime.Now,
                EventId = dto.EventId,
                //ReplyId = dto.ReplyId
            };

            return comment;
        }

        public CommentDTO CreateComment(EventComment eventComment)
        {

            var comment = new CommentDTO() {
                CommentId = eventComment.EventCommentId,
                Username = eventComment.User.UserName,
                CommentText = eventComment.CommentText,
                Since = eventComment.Since
            };

            return comment;
        }

        public SingleEventDTO CreateForSingle(Event evnt)
        {
            return new SingleEventDTO()
            {
                EventId = evnt.EventId,
                Name = evnt.Name,
                Location = evnt.Location,
                Categories = evnt.EventCategory
                .Where(x => x.EventId == evnt.EventId)
                .Select(x => CategoryFactory.CategoryFactory.Create(x.Category)).ToList(),
                VolunteersLimit = evnt.VolunteersLimit,
                Priority = evnt.Priority,
                Description = evnt.Description,
                Created = DateTime.Now,
                StartTime = evnt.Start,
                EndTime = evnt.End,
                ImageUrl = evnt.ImageUrl,
                Participants = evnt.UserInEvents.Where(x=>x.To == null)
                    .Select(x => UserFactory.UserFactory.Create(x.User)).ToList(),
                Organizer = UserFactory.UserFactory
                .Create(evnt.UserInEvents
                .Where(x => x.ParticipantRole == "organizer")
                .Select(x=>x.User).FirstOrDefault())
            };
        }
    }
}
