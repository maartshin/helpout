﻿using BL.DTOs;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.ObjectFactories.UserFactory
{
    public class UserFactory
    {
        public static UserDTO Create(User user)
        {
            return new UserDTO()
            {
                UserId = user.UserId,
                AspNetUserId = user.AspNetUserId,
                UserName = user.UserName
            };
        }

        public static User Create(UserDTO userDTO)
        {
            return new User()
            {
                UserId = userDTO.UserId,
                AspNetUserId = userDTO.AspNetUserId,
                UserName = userDTO.UserName,
                Since = DateTime.Now
            };
        }

        


    }
}
