﻿using BL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.ObjectFactories.UserDataFactory
{
    public class UserDataFactory
    {
        public static UserDataDTO Create(UserDataDTO userDTO)
        {
            return new UserDataDTO()
            {
                FirstName = userDTO.FirstName,
                LastName = userDTO.LastName,
                Email = userDTO.Email,
                Phone = userDTO.Phone,
                BirthDate = userDTO.BirthDate
            };
        }
    }
}
