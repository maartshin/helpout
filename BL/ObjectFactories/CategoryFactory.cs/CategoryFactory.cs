﻿using BL.DTOs;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.ObjectFactories.CategoryFactory
{
    public class CategoryFactory
    {
        public static CategoryDTO Create(Category category)
        {
           
            return category == null ? 
                null : 
                new CategoryDTO()
                {
                    CategoryId = category.CategoryId,
                    name = category.name,
                };
        }

        public static Category Create(CategoryDTO categoryDTO)
        {
            return new Category()
            {
                CategoryId = categoryDTO.CategoryId,
                name = categoryDTO.name,
            };
        }
    }
}
