﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTOs
{
    public class FilterDTO
    {
        public int[] categories { get; set; }
        public string searchString { get; set; }
        public int elementsOnPage { get; set; }
        public int pageNumber { get; set; }
    }
}
