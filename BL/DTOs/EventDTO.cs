﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTOs
{
    public class EventDTO
    {
        public int EventId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public int Priority { get; set; }
        public int VolunteersLimit { get; set; }
        public DateTime Created { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string ImageUrl { get; set; }

        public int ParticipantsId { get; set; }
        public List<CategoryDTO> Categories { get; set; }
        public int[] CategoryIds { get; set; }

    }
}
