﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTOs
{
    public class CommentDTO
    {
        public int CommentId { get; set; }
        public int ReplyId { get; set; }
        public int UserId { get; set; }
        public int EventId { get; set; }
        public string Username { get; set; }
        public string CommentText { get; set; }
        public DateTime Since { get; set; }
        public DateTime To { get; set; }
        public List<CommentDTO> Replies { get; set; }
    }
}
