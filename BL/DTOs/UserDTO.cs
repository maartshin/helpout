﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTOs
{
    public class UserDTO : UserDataDTO
    {
        public int UserId { get; set; }
        public string AspNetUserId { get; set; }
        public string UserName { get; set; }
    }
}
