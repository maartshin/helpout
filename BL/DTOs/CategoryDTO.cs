﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTOs
{
    public class CategoryDTO
    {
        public int CategoryId { get; set; }
        public string name { get; set; }
    }
}
