﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTOs
{
    public class EventsDTO
    {
        public int total { get; set; }
        public List<EventDTO> events { get; set; }
    }
}
