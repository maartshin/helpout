﻿using BL.DTOs;
using Domain;
using Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services.EventService
{
    public interface IEventService
    {
        List<EventDTO> GetList();
        List<UserDTO> GetParticipants(int eventId);
        UserDTO GetEventOrganizer(int eventId);
        List<Category> GetEventCategories(int eventId);
        EventDTO AddEvent(EventDTO dto, string userId, int[] categoryIds);
        SingleEventDTO GetEvent(int id);
        bool Participate(int eventId, string userId);
        List<Category> GetAllCategories();
        List<CommentDTO> GetComments(int eventId);
        CommentDTO PostComment(CommentDTO comment, string userId);
        bool isParticipant(int eventId, string userId);
        int ParticipantCount(int eventId);
        EventsDTO getFilteredEvents(FilterDTO filter);
        bool eventExists(int eventId);
    }
}
