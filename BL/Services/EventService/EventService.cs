﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL.DTOs;
using Interfaces;
using BL.ObjectFactories.EventFactory;
using Domain;
using Domain.Enum;
using BL.ObjectFactories.UserFactory;

namespace BL.Services.EventService
{
    public class EventService : IEventService
    {
        private readonly EventFactory _eventFactory;
        private readonly IUow _uow;

        public EventService(IUow uow)
        {
            _uow = uow;
            _eventFactory = new EventFactory();
        }

        public List<EventDTO> GetList()
        {
            return _uow.Events.All.Select(x => _eventFactory.Create(x)).ToList();
        }

        public EventDTO AddEvent(EventDTO dto, string userId, int[] categoryIds)
        {
            var domain = _eventFactory.Create(dto);
            var ret = _uow.Events.Add(domain);
            int creatorId = _uow.Users.All.Where(x => x.AspNetUserId == userId).Select(x=>x.UserId).FirstOrDefault();
            if (userId == null)
                throw new Exception("userid is null");

            _uow.UserInEvent.Add(new UserInEvent
            {
                ParticipantRole = "organizer",
                EventId = ret.EventId,
                UserId = creatorId,
                Since = DateTime.Now
            });

            foreach (var id in categoryIds)
            {
                _uow.EventCategory.Add(new EventCategory
                {
                    EventId = ret.EventId,
                    CategoryId = id

                });
            }
            _uow.SaveChanges();
            return _eventFactory.Create(ret);
        }

        public SingleEventDTO GetEvent(int id)
        {
            return _eventFactory.CreateForSingle(_uow.Events.Find(id));
        }

        public List<Category> GetEventCategories(int eventId)
        {
            return _uow.EventCategory.All.Where(x => x.EventId == eventId)
                .Select(x => x.Category).ToList();
        }

        public List<UserDTO> GetParticipants(int eventId)
        {
            var evnt = _uow.Events.Find(eventId);
            if (evnt == null)
            {
                return null;
            }
            return _uow.UserInEvent.All.Where(x => x.EventId == eventId && x.To==null)
                .Select(x => UserFactory.Create(x.User)).ToList();
        }

        public int ParticipantCount(int eventId)
        {

            var participants = GetParticipants(eventId);

            return (participants == null) ? -1 : participants.Count();
        }


        public UserDTO GetEventOrganizer(int eventId)
        {
            return _uow.UserInEvent.All
                .Where(x => x.EventId == eventId && x.ParticipantRole == "organizer")
                .Select(x => UserFactory.Create(x.User)).FirstOrDefault();
        }

        public bool Participate(int eventId, string userId)
        {
            var participant = _uow.Users.FindByStringId(userId);
            
            if (participant == null|| !eventExists(eventId))
                return false;

            int participantId = participant.UserId;

            if(!isParticipant(eventId, userId))
            {
                _uow.UserInEvent.Add(new UserInEvent()
                {
                    EventId = eventId,
                    UserId = participantId,
                    ParticipantRole = "participant",
                    Since = DateTime.Now
                });
            }
            else
            {
                var userInEvent = _uow.UserInEvent.All.Where(x => x.UserId == participantId && x.EventId == eventId && x.To == null)
                    .FirstOrDefault();
                userInEvent.To = DateTime.Now;
            }

            _uow.SaveChanges();
            return true;
        }

        public List<Category> GetAllCategories()
        {
            return _uow.Category.All.ToList();
        }

        public List<CommentDTO> GetComments(int eventId) {
            var comments = _uow.EventComment.All.Where(x => x.EventId == eventId).ToList();
            var dtos = comments.Select(x => _eventFactory.CreateComment(x)).ToList();

            return dtos;
        }

        public CommentDTO PostComment(CommentDTO comment, string AspUserId)
        {
            var cmnt = comment;
            int userId = _uow.Users.All
                .Where(x => x.AspNetUserId == AspUserId)
                .Select(x=>x.UserId).FirstOrDefault();
            cmnt.UserId = userId;

            var commentE = _uow.EventComment.Add(_eventFactory.CreateComment(cmnt));
            _uow.SaveChanges();

            return _eventFactory.CreateComment(commentE);
        }

        public bool isParticipant(int eventId, string userId)
        {
            var userInEvent = _uow.UserInEvent.All
                .Where(x => x.User.AspNetUserId == userId && x.EventId == eventId)
                .ToList();

            foreach(var item in userInEvent)
            {
                if (item.To == null)
                    return true;
            }
            return false;
        }

        public bool eventExists(int eventId)
        {
            var evnt = _uow.Events.Find(eventId);
            return (evnt == null) ? false : true;

        }

        public EventsDTO getFilteredEvents(FilterDTO filter)
        {
            var filteredEvents = new List<Event>();
            if(filter.categories.Length == 0)
            {
                filteredEvents = _uow.Events.All;
            }
            else {
                foreach(var id in filter.categories)
                    {
                        var evnts = _uow.EventCategory.All.Where(x => x.CategoryId == id)
                            .Select(x=>x.Event).ToList();
                        filteredEvents = filteredEvents.Union(evnts).ToList();
                    }
            }

            filteredEvents = filteredEvents
                .Where(x => x.End > DateTime.Now)
                .OrderBy(x=>x.Priority).ToList();

            if(filter.searchString!=null && filter.searchString!="")
            {
                filteredEvents = filteredEvents.Where(x => x.Name.ToLower()
                .Contains(filter.searchString.ToLower())).ToList();
            }

            int count = filteredEvents.Count();
            int start = (filter.pageNumber - 1) * filter.elementsOnPage;
            int end = start + filter.elementsOnPage;

            int elements = filter.elementsOnPage;

            if (end > count)
                elements = count - start;

            var eventsOnPage = filteredEvents.GetRange(start, elements);
            var eventDTOs = eventsOnPage.Select(x => _eventFactory.Create(x)).ToList();

            return new EventsDTO()
            {
                events = eventDTOs,
                total = count
            };
        }
    }
}
