﻿using BL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Security.Principal;
using Domain;

namespace BL.Services.UserService
{
    public interface IUserService
    {
        UserDTO AddUser(UserDTO dto);
        List<UserDTO> GetAllUsers();
        UserDTO GetUserByAspNetId(string id);
        UserDTO GetUserWithData(string id);
        void EditUserData(int userId, UserDTO dto);
        List<EventDTO> GetUserEvents(string userAspNetId);
        bool userExists(string aspNetId);
    }
}
