﻿using BL.Services.UserService;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using BL.DTOs;
using BL.ObjectFactories.UserFactory;
using Domain;
using BL.ObjectFactories.UserDataFactory;
using System.Reflection;
using BL.ObjectFactories.EventFactory;

namespace BL.UserService.Services
{
    public class UserService : IUserService
    {
        private readonly EventFactory _eventFactory;
        private readonly IUow _uow;

        public UserService(IUow uow)
        {
            _uow = uow;
            _eventFactory = new EventFactory();
        }

        public UserDTO AddUser(UserDTO dto)
        {
            var domain = UserFactory.Create(dto);

            var newUser = _uow.Users.Add(domain);
            _uow.SaveChanges();

            dto.UserId = newUser.UserId;
            AddNewUserDataTypes(dto);
            AddUserDataRecords(dto);
            
            return UserFactory.Create(newUser);
        }

        public List<UserDTO> GetAllUsers()
        {
            return _uow.Users.All.Select(x => UserFactory.Create(x)).ToList();
        }

        public UserDTO GetUserByAspNetId(string id)
        {

            //var user = _uow.Users.FindByStringId(id);
            var user = _uow.Users.All.Where(x => x.AspNetUserId == id).FirstOrDefault();
            return UserFactory.Create(user);
        }

        public UserDTO GetUserWithData(string id)
        {
            var userDTO = GetUserByAspNetId(id);
            var userData = _uow.UserData.All.Where(x => x.UserId == userDTO.UserId).ToArray();

            var userDTOProperties = userDTO.GetType().GetProperties();

            foreach (var data in userData)
            {
                // finds user's propery name by UserData UserDataTypeId
                var propertyName = _uow.UserDataType.All.Where(x => x.UserDataTypeId == data.UserDataTypeId).FirstOrDefault().DataType;
                foreach (var property in userDTOProperties)
                {
                    if (propertyName == property.Name)
                    {
                        // finds record value (user's property value) of UserDatat
                        var propertyValue = _uow.Record.All.Where(x => x.RecordId == data.RecordId).FirstOrDefault().Value;
                        // sets record value to corresponding property
                        SetValueToProperty(userDTO, property, propertyValue);
                    }
                }
            }

            return userDTO;
        }

        public void EditUserData(int userId, UserDTO dto)
        {
            
        }

        private void AddNewUserDataTypes(UserDTO dto)
        {
            var data = UserDataFactory.Create(dto);
            foreach (var property in data.GetType().GetProperties())
            {
                if (!_uow.UserDataType.All.Any(x => x.DataType == property.Name))
                {
                    _uow.UserDataType.Add(new UserDataType{ DataType = property.Name });
                }
            }
            _uow.SaveChanges();
        }

        private void AddUserDataRecords(UserDTO dto)
        {
            var data = UserDataFactory.Create(dto);
            foreach (var record in data.GetType().GetProperties())
            {
                var value = record.GetValue(data);
                if (value != null)
                {
                    var newRecord = _uow.Record.Add(new Record { Value = value.ToString() });
                    _uow.SaveChanges();

                    var userDataType = _uow.UserDataType.All.Where(x => x.DataType == record.Name).FirstOrDefault();
                    var userData = _uow.UserData.Add(new UserData
                    {
                        RecordId = newRecord.RecordId,
                        UserDataTypeId = userDataType.UserDataTypeId,
                        UserId = dto.UserId,
                        Since = DateTime.Now
                    });
                    _uow.SaveChanges();
                }
            }
           
        }

        public List<EventDTO> GetUserEvents(string id)
        {
            var user = _uow.Users.FindByStringId(id);
            return user.UserInEvents
                .Where(x => x.To == null && x.ParticipantRole == "participant")
                .Select(x => _eventFactory.Create(x.Event)).ToList();
        }

        public bool userExists(string aspNetId) {
            var user = _uow.Users.FindByStringId(aspNetId);
            if (user == null)
            {
                return false;
            }
            return true;
        }

# region Helpers

        private static void SetValueToProperty(Object obj, PropertyInfo property, string value)
        {
            obj.GetType().GetProperty(property.Name).SetValue(obj, value.ToString());
        }

#endregion

    }
}
