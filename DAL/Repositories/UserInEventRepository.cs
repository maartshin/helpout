﻿using Domain;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DAL.Repositories
{
    public class UserInEventRepository : EFRepository<UserInEvent>, IUserInEventRepository
    {
        public UserInEventRepository(IAppDbContext dbContext) : base(dbContext)
        {
        }
    }
}
