﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Interfaces;

namespace DAL.Repositories
{
    public class UserRepository : EFRepository<User>, IUserRepository
    {
        public UserRepository(IAppDbContext dbContext) : base(dbContext)
        {
        }

        public User FindByStringId(string id)
        {
            //var query = RepositoryDbSet.AsQueryable();
            var b = RepositoryDbSet.Any(x => x.AspNetUserId == id);
            return RepositoryDbSet.Where(x => x.AspNetUserId == id).FirstOrDefault();
        }
    }
}
