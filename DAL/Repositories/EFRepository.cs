﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;

namespace DAL.Repositories
{
    public class EFRepository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        protected DbContext RepositoryDbContext;
        protected DbSet<TEntity> RepositoryDbSet;

        public EFRepository(IAppDbContext dbContext)
        {
            RepositoryDbContext = dbContext as DbContext;
            if (RepositoryDbContext == null)
            {
                throw new ArgumentNullException(nameof(dbContext));
            }

            RepositoryDbSet = RepositoryDbContext.Set<TEntity>();

            if (RepositoryDbSet == null)
            {
                throw new NullReferenceException(nameof(RepositoryDbSet));
            }
        }

        public List<TEntity> All => RepositoryDbSet.ToList();
        public TEntity Find(int id)
        {
            return RepositoryDbSet.Find(id);
        }

        public void Remove(int id)
        {
            Remove(Find(id));
        }

        public void Remove(TEntity entity)
        {
            DbEntityEntry dbEntityEntry = RepositoryDbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Deleted)
            {
                dbEntityEntry.State = EntityState.Deleted;
            }
            else
            {
                RepositoryDbSet.Attach(entity);
                RepositoryDbSet.Remove(entity);
            }
        }

        public TEntity Add(TEntity entity)
        {
            DbEntityEntry dbEntityEntry = RepositoryDbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
                return entity;
            }
            return RepositoryDbSet.Add(entity);
        }

        public void Update(TEntity entity)
        {
            DbEntityEntry dbEntityEntry = RepositoryDbContext.Entry(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                RepositoryDbSet.Attach(entity);
            }
            dbEntityEntry.State = EntityState.Modified;
        }

        public int SaveChanges()
        {
            return RepositoryDbContext.SaveChanges();
        }
    }
}
