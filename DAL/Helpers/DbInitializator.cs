﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;

namespace DAL.Helpers
{
    public  class DbInitializator : DropCreateDatabaseIfModelChanges<AppDbContext>
    {
        protected override void Seed(AppDbContext context)
        {
            context.Users.Add(new User()
            {
                UserName = "jaak pauk",
                AspNetUserId = "1",
                Since = DateTime.Now
            });

            context.Categories.Add(new Category()
            {
                name = "sport"
            });

            context.Categories.Add(new Category()
            {
                name = "talgud"
            });

            context.Categories.Add(new Category()
            {
                name = "18+"
            });

            context.SaveChanges();
        }
    }
}
