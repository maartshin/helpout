﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Helpers
{
    public class EFRepositoryProvider : IRepositoryProvider
    {
        private readonly IAppDbContext _appDataContext;
        private readonly IRepositoryFactory _repositoryFactory;

        public EFRepositoryProvider(IAppDbContext appDataContext, IRepositoryFactory repositoryFactory)
        {
            _appDataContext = appDataContext;
            _repositoryFactory = repositoryFactory;
        }

        protected Dictionary<Type, object> Repositories { get; } = new Dictionary<Type, object>();


        public TRepositoryInterface GetCustomRepository<TRepositoryInterface>()
        {
            return GetRepository<TRepositoryInterface>(_repositoryFactory.GetCustomRepositoryFactory<TRepositoryInterface>());
        }

        public IRepository<TEntity> GetStandardRepository<TEntity>() where TEntity : class
        {
            return GetRepository<IRepository<TEntity>>(_repositoryFactory.GetStandardRepositoryFactory<TEntity>());
        }

        private TRepository GetRepository<TRepository>(Func<IAppDbContext, object> factory)
        {
            object repositoryObject;
            Repositories.TryGetValue(typeof(TRepository), out repositoryObject);

            if (repositoryObject != null)
            {
                return (TRepository)repositoryObject;
            }

            return MakeRepository<TRepository>(factory, _appDataContext);
        }

        private TRepository MakeRepository<TRepository>(Func<IAppDbContext, object> factory, IAppDbContext appDbContext)
        {
            if (factory == null)
            {
                throw new ArgumentNullException($"No factory found for repository {typeof(TRepository).FullName}");
            }
            var repository = (TRepository)factory(appDbContext);
            Repositories[typeof(TRepository)] = repository;
            return repository;
        }
    }
}
