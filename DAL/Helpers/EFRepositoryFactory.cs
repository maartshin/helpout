﻿using DAL.Repositories;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Helpers
{
    public class EFRepositoryFactory : IRepositoryFactory
    {
        private static readonly Dictionary<Type, Func<IAppDbContext, object>> _customRepositoryFactory = new Dictionary<Type, Func<IAppDbContext, object>>()
        {
            { typeof(IUserRepository), appDataContext => new UserRepository(appDataContext) }
        };

        public Func<IAppDbContext, object> GetCustomRepositoryFactory<TRepositoryInterface>()
        {
            Func<IAppDbContext, object> customRepositoryFactory;
            _customRepositoryFactory.TryGetValue(typeof(TRepositoryInterface), out customRepositoryFactory);
            return customRepositoryFactory;
        }

        public Func<IAppDbContext, object> GetStandardRepositoryFactory<TEntity>() where TEntity : class
        {
            return appDbContext => new EFRepository<TEntity>(appDbContext);
        }
    }
}
