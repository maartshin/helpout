﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Helpers;
using Domain;
using Microsoft.Win32;
using Interfaces;

namespace DAL
{
    public class AppDbContext : DbContext, IAppDbContext
    {
        public AppDbContext() : base("name=HelpOutDatabase")
        {
            Database.SetInitializer(new DbInitializator());
        }

        public DbSet<User> Users { get; set; }
        public DbSet<UserData> UserData { get; set; }
        public DbSet<UserDataType> UserDataType { get; set; }
        public DbSet<Record> Record { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<UserInEvent> UserInEvent { get; set; }
        public DbSet<EventCategory> EventCategory { get; set; }
        public DbSet<EventComment> EventComment { get; set; }
        public DbSet<Category> Categories { get; set; }

    }
}
