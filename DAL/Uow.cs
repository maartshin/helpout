﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using System.Data.Entity;
using DAL.Repositories;

namespace DAL
{
    public class Uow : IUow
    {
        private readonly IAppDbContext _appDataContext;
        private readonly IRepositoryProvider _repositoryProvider;

        public Uow(IAppDbContext appDataContext, IRepositoryProvider repositoryProvider)
        {
            _appDataContext = appDataContext;
            _repositoryProvider = repositoryProvider;
        }

        public IRepository<Event> Events => GetStandardRepository<Event>();
        public IRepository<UserInEvent> UserInEvent => GetStandardRepository<UserInEvent>();
        public IRepository<EventCategory> EventCategory => GetStandardRepository<EventCategory>();
        public IRepository<Category> Category => GetStandardRepository<Category>();
        public IRepository<EventComment> EventComment => GetStandardRepository<EventComment>();
        public IRepository<UserDataType> UserDataType => GetStandardRepository<UserDataType>();
        public IRepository<Record> Record => GetStandardRepository<Record>();
        public IRepository<UserData> UserData => GetStandardRepository<UserData>();

        public IUserRepository Users => GetCustomRepository<IUserRepository>();

        private IRepository<TEntity> GetStandardRepository<TEntity>() where TEntity : class
        {
            return _repositoryProvider.GetStandardRepository<TEntity>();
        }

        private TRepositoryInterface GetCustomRepository<TRepositoryInterface>()
        {
            return _repositoryProvider.GetCustomRepository<TRepositoryInterface>();
        }

        public int SaveChanges()
        {
            return ((DbContext)_appDataContext).SaveChanges();
        }
    }
}
