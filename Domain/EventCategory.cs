﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class EventCategory
    {

        public int EventCategoryId { get; set; }
        
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }

        public int EventId { get; set; }
        public virtual Event Event { get; set; }
    }
}
