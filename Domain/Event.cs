﻿using Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Event
    {
        public int EventId { get; set; }
        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        public string Name { get; set; }
        [Required]
        [Range(0,5)]
        public int Priority { get; set; }
        [Required]
        [MinLength(2)]
        [MaxLength(300)]
        public string Location { get; set; }
        [Required]
        [Range(0,100000)]
        public int VolunteersLimit { get; set; }
        [Required]
        public DateTime Created { get; set; }
        [Required]
        public DateTime Start { get; set; }
        [Required]
        public DateTime End { get; set; }
        [Required]
        [MaxLength(2000)]
        public string Description { get; set; }
        [Required]
        [MaxLength(500)]
        public string ImageUrl { get; set; }

        public virtual List<UserInEvent> UserInEvents { get; set; } = new List<UserInEvent>();
        public virtual List<Feedback> FeedbackFromUsers { get; set; } = new List<Feedback>();
        public virtual List<EventComment> EventComment { get; set; } = new List<Domain.EventComment>();
        public virtual List<EventCategory> EventCategory { get; set; } = new List<EventCategory>();
    }
}
