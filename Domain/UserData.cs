﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class UserData
    {
        public int UserDataId { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }

        public int UserDataTypeId { get; set; }
        public virtual UserDataType UserDataType { get; set; }

        public int RecordId { get; set; }
        public virtual Record Record { get; set; }

        public DateTime Since { get; set; }
        public DateTime? To { get; set; }


    }
}
