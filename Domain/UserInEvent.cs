﻿using Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class UserInEvent
    {
        public int UserInEventId { get; set; }
        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        public string ParticipantRole { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public int EventId { get; set; }
        public virtual Event Event { get; set; }

        [Required]
        public DateTime Since { get; set; }
        public DateTime? To { get; set; }
    }
}
