﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Record
    {
        public int RecordId { get; set; }
        [Required]
        [MinLength(1)]
        [MaxLength(1000)]
        public string Value { get; set; }

    }
}
