﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class UserDataType
    {
        public int UserDataTypeId { get; set; }
        [Required]
        [MinLength(1)]
        [MaxLength(500)]
        public string DataType { get; set; }
    }
}
