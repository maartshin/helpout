﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Feedback
    {
        public int FeedbackId { get; set; }
        [Required]
        [MinLength(1)]
        [MaxLength(2000)]
        public string FeedbackText { get; set; }
        [Required]
        public DateTime Since { get; set; }
        public DateTime To { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public int EventId { get; set; }
        public virtual Event Event { get; set; }
    }
}
