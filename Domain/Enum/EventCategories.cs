﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Enum
{
    public enum EventCategories
    {
        // test commit -- Mikk
        Farming,
        Government,
        Movie,
        Music,
        Sport,
        Theatre
    }
}
