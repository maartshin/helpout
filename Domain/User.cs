﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class User
    {
        public int UserId { get; set; }
        public string AspNetUserId { get; set; }
        [Required]
        [MaxLength(25)]
        [MinLength(4)]
        public string UserName { get; set; }
        /*public DateTime Since
        {
            get { return Since; }
            set
            {
                value = DateTime.Now;
            }
        }*/
        [Required]
        public DateTime Since { get; set; }
        public DateTime? To { get; set; }

        public virtual List<UserInEvent> UserInEvents { get; set; } = new List<UserInEvent>();
        public virtual List<UserData> UserData { get; set; } = new List<UserData>();
        public virtual List<EventComment> Useromments { get; set; } = new List<EventComment>();
        public virtual List<Feedback> UserFeetbacksToEvent { get; set; } = new List<Feedback>();
    }
}
