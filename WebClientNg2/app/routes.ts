import { Routes } from '@angular/router';

import { EventsListComponent } from './events/eventsList.cmp';
import { EventViewComponent } from './events/eventView.cmp';
import { EventAddComponent } from './events/eventAdd.cmp';
import { AccountCreateComponent } from './account/accountCreate.cmp';
import { AccountProfileComponent } from './account/accountProfile.cmp';
import { LoginLogoutComponent } from './account/loginLogout.cmp';

export const routes: Routes = [
    { path: 'main', component: EventsListComponent },
    { path: 'eventsList', component: EventsListComponent },
    { path: 'event/new', component: EventAddComponent },
    { path: 'events/:id', component: EventViewComponent },
    { path: 'accountCreate', component: AccountCreateComponent },
    { path: 'accountProfile', component: AccountProfileComponent },
    { path: 'login', component: LoginLogoutComponent },
    { path: '', redirectTo: 'main', pathMatch: 'full' }
];