﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.cmp';
import { RouterModule } from '@angular/router';
import { routes } from './routes';

import { EventService } from './services/event.srv';
import { AccountService } from './services/account.srv';

import { EventsListComponent } from './events/eventsList.cmp';
import { EventViewComponent } from './events/eventView.cmp';
import { EventAddComponent } from './events/eventAdd.cmp';
import { UpComingEventsComponent } from './events/upComingEvents.cmp';
import { NamePipe } from './events/namePipe';


import { AccountCreateComponent } from './account/accountCreate.cmp';
import { AccountProfileComponent } from './account/accountProfile.cmp';
import { LoginLogoutComponent } from './account/loginLogout.cmp';

import { HeaderComponent } from './template/header.cmp'


@NgModule({
    imports: [BrowserModule,
        HttpModule,
        FormsModule,
        RouterModule.forRoot(routes, { useHash: true })],
    declarations: [AppComponent, EventsListComponent, EventViewComponent,
        AccountCreateComponent, AccountProfileComponent, LoginLogoutComponent,
        EventAddComponent, HeaderComponent, NamePipe, UpComingEventsComponent],
    providers: [EventService, AccountService],
    bootstrap: [AppComponent]
})
export class AppModule {
}