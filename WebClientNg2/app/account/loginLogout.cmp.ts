﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from '../services/account.srv';
import { HeaderComponent } from '../template/header.cmp';
import { LoginValidation } from '../helpers/validations/loginValidation';
import * as $ from 'jquery';

declare var $: any;

@Component({
    selector: 'login-view',
    templateUrl: 'app/account/loginLogout.view.html',
    providers: [HeaderComponent],
    styleUrls: ['app/account/loginLogout.css']
})

export class LoginLogoutComponent {

    username: string;
    password: string;
    loginUser: any;
    errorMessage: string = "";

    constructor(private accountService: AccountService,
                private router: Router,
                private headerComponent: HeaderComponent) {

        //for validation
        this.loginUser = {
            'username': '',
            'password': ''
        }
    }

    private login(): void {
        console.log("login");
        let authentication = new LoginValidation(this.loginUser);
        if (!authentication.isValid) {
            this.errorMessage = authentication.message;
            $('#div-login-error').addClass('show-form-error')
        } else {
            this.accountService.login(this.loginUser.username, this.loginUser.password)
                .then(() => {
                    this.headerComponent.updateHeader();
                    console.log('logging in');
                    this.router.navigateByUrl('/main');
                }).catch(error => {
                    authentication.denyUser();
                    this.errorMessage = authentication.message;
                    $('#div-login-error').addClass('show-form-error');
                });
        }
        
    }

}