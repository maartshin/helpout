﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AccountService } from '../services/account.srv';
import { RegistrationValidation } from '../helpers/validations/registrationValidation';

import * as $ from 'jquery';

declare var $: any;


@Component({
    selector: 'account-create-view',
    templateUrl: 'app/account/accountCreate.view.html'
})

export class AccountCreateComponent {

    user: any;
    errorMessage: string = "";

    constructor(private accountService: AccountService,
                private router: Router) {

        //for validation
        this.user = {
            'FirstName': '',
            'LastName': '',
            'Email': '',
            'Phone': '',
            'Username': '',
            'Password': '',
            'ConfirmPassword': '',
            'BirthDate': ''
        };
    }

    private saveNewAccount(): void{
        console.log(this.user);
        let validation = new RegistrationValidation(this.user);
        if (!validation.isValid) {
            $('#div-regitration-error').css("display", "block");
            this.errorMessage = validation.message;
        } else {
            this.accountService.saveNewUser(this.user)
                .then(() => this.router.navigateByUrl('/main'))
        }
    }
}