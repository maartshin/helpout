﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AccountService } from '../services/account.srv';
import * as $ from 'jquery';

declare var $: any;
@
Component({
    selector: 'account-profile-view',
    templateUrl: 'app/account/accountProfile.view.html',
    styleUrls: ['app/account/accountProfile.css']
})

export class AccountProfileComponent implements OnInit{

    account: any;

    constructor(private router: Router, private accountService: AccountService) {
        this.account = {};
    }

    ngOnInit(): void {
        if (!sessionStorage.getItem('accessToken')) {
            this.router.navigateByUrl('/login');
            console.log("Create event denied: User not logged in.")
            return;
        }
        this.accountService.getloggedInUserInfo(sessionStorage.getItem('accessToken'))
            .then(result => this.account = result)
            .then(() => console.log(this.account));

        this.initJqueryEvents();
    }

    confirmEdit() {
        console.log("edit");
        this.accountService.editAccount(this.account);
    }


    initJqueryEvents() {

        $('button.changeButton').click(() => {
            let display = $('.userData').css('display');
            if (display === 'none') {
                $('.pastEventTable').css('display', 'none');
                $('.comingEventTable').css('display', 'none');
                $('.userData').css('display', 'block');
            }
            else {
                $('.userData').css('display', 'none');
            }
        });

    }
}