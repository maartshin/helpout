﻿import { FormValidation } from './formValidation';


export class LoginValidation extends FormValidation {
    constructor(loginObject: any) {
        super(loginObject);
        this.isAllFilled();
    }

    public denyUser(): void {
        this.isValid = false;
        this.message = "Vale kasutajanimi või parool";
    }

}