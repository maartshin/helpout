﻿import { FormValidation } from './formValidation';

export class RegistrationValidation extends FormValidation {
    constructor(registrationObject: any) {
        super(registrationObject);
        this.passwordConfirmation();
        this.emailValidation();
        this.isAllFilled();
    }

    private passwordConfirmation(): void {
        if (this.formObject.ConfirmPassword !== this.formObject.Password) {
            this.isValid = false;
            this.message = "Salasõnad ei ole identsed."
            
        }
    }

    private emailValidation(): void {
        let email: string = this.formObject.Email.toString();
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(email)) {
            this.isValid = false;
            this.message = "Ebakorrektne email."
        };
    }
}