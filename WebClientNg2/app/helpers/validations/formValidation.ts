﻿export class FormValidation {

    isValid: boolean = true;
    message: string = "";
    formObject: any;

    constructor(public obj: any) {
        this.formObject = obj;
    }

    protected isAllFilled(): void {
        Object.keys(this.formObject).forEach(x => {
            if (this.formObject[x].toString() === '') {
                this.isValid = false;
                this.message = 'Täida kõik väljad.';
            }
        });
    }

    private getMessage(): string {
        return this.message;
    }

}