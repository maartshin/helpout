﻿import { FormValidation } from './formValidation';
import { Evnt } from '../../services/event.srv';

export class EventValidation extends FormValidation {
    constructor(eventObject: any) {
        super(eventObject);
        this.formObject = <Evnt>this.formObject;
        
        this.validateDateTime();
        this.isAllFilled();
    }

    private validateDateTime(): void{
        let start = new Date(this.formObject.StartTime).getTime();
        let end = new Date(this.formObject.EndTime).getTime();
        if (end <= start) {
            this.isValid = false;
            this.message = 'Lõppaeg peab olema algusajast hilisem.';
        }
        if (start < Date.now()) {
            this.isValid = false;
            this.message = 'Algusaeg ei tohi olla minevikus.';
        }
    }
}