﻿import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { AccountService } from '../services/account.srv';

@Component({
    selector: 'header',
    templateUrl: 'app/template/header.view.html',
    styleUrls: ['app/template/header.css']
})

export class HeaderComponent implements OnInit{
    username: string;
    isLoggedIn: boolean = false;


    constructor(private accountService: AccountService,
                private ref: ChangeDetectorRef) { }

    ngOnInit(): void {
        this.updateHeader();
    }

    updateHeader() {
        this.isLoggedIn = this.accountService.isLoggedIn();
        console.log('view updated');
    }

    logOff() {
        console.log('logoff');
        this.accountService.logoff();
    }


}