﻿import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Injectable } from '@angular/core';

export class Evnt {
    CategoryIds: any;
    categories: any;
    eventId: any;
    startTime: any;
    endTime: any;
    priority: number;
    name: string;
    organizer: any;
    participants: any;
    volunteersLimit: any;
}

export class Category {
    name: string;
    selected: boolean;
    categoryId: number;
}

export class Comment {
    eventId: number;
    since: string;

    constructor(private commentText:string) {
    }
}

@Injectable()
export class EventService {

    constructor(private http: Http) { }

    getEvents(): Promise<Evnt[]> {
        return this.http.get('api/events')
            .toPromise()
            .then((response: Response) => response.json());
    }

    getEvent(id: number): Promise<Evnt> {
        return this.http.get('api/events/' + id)
            .toPromise()
            .then((response: Response) => response.json());
    }

    getComments(id: number): Promise<Comment[]> {
        return this.http.get('api/events/' + id+ '/comments')
            .toPromise()
            .then((response: Response) => response.json());
    }

    addComment(id: number, comment: Comment): Promise<void> {
        let options = this.getPostOptions(sessionStorage.getItem('accessToken'));

        return this.http.post('api/events/' + id + '/comments', comment, options)
            .toPromise()
            .then((response: Response) => response.json());
    }


    addEvent(evnt: Evnt, accessToken: string): Promise<Evnt> {

        let options = this.getPostOptions(accessToken);

        return this.http.post('api/events/', evnt, options)
            .toPromise()
            .then((response: Response) => response.json());
    }

    getAllCategories(): Promise<Category[]> {
        return this.http.get('api/events/categories')
            .toPromise()
            .then((response: Response) => response.json());
    }

    participantCount(id:number): Promise<number> {
        return this.http.get('api/events/'+id+"/participantscount")
            .toPromise()
            .then((response: Response) => response.json());
    }

    getEventParticipants(id: number): Promise<number> {
        return this.http.get('api/events/' + id + "/Participants")
            .toPromise()
            .then((response: Response) => response.json());
    }

    isParticipant(id: number, accessToken: string): Promise<boolean> {
        let options = this.getPostOptions(accessToken);

        return this.http.get('api/events/' + id + '/isparticipant', options)
            .toPromise()
            .then((response: Response) => response.json());
    }

    participate(id:number, accessToken:string): Promise<any>{
        let options = this.getPostOptions(accessToken);
        return this.http.get('api/events/' + id + '/Participate', options)
            .toPromise()
            .then((response: Response) => response.json());
    }

    getPageEvents(filterData) {
        return this.http.post('api/events/getevents', filterData)
            .toPromise()
            .then((response: Response) => response.json());
    }
    getUpComingEvents(accessToken: string): Promise<any> {

        let options = this.getPostOptions(accessToken);
        return this.http.get('api/Users/Events', options)
            .toPromise()
            .then((response: Response) => response.json());
    }

    getPostOptions(accessToken:string) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        headers.append('Authorization', 'Bearer ' + accessToken);
        let options = new RequestOptions({ headers: headers });
        return options;
    }

}