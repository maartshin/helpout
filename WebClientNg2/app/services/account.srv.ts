﻿import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Injectable } from '@angular/core';

export class Account {

}

@Injectable()
export class AccountService {

    constructor(private http: Http) { }

    saveNewUser(newAccount: Object): Promise<void>{
        return this.http
            .post('api/account/register', newAccount)
            .toPromise()
            .then(() => <void>null);
    }

    login(username: string, password: string): Promise<void> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        let data = 'grant_type=password&username=' + username + '&password=' + password;
        let option = new RequestOptions({ headers: headers });

        return this.http.post('http://localhost:56265/Token', data, option).toPromise()
            .then(response => {
                if (response.status == 200) {
                    let result = response.json();
                    sessionStorage.setItem('accessToken', result.access_token);
                }
            });
    }

    isLoggedIn() {
        return (sessionStorage.getItem('accessToken')) ? true : false;
    }

    logoff() {
        sessionStorage.removeItem('accessToken');
    }

    getloggedInUserInfo(accessToken: string): Promise<Account> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        headers.append('Authorization', 'Bearer ' + accessToken);
        let options = new RequestOptions({ headers: headers });
        return this.http.get('api/Users/LoggedInUser', options)
            .toPromise()
            .then((response: Response) => response.json());
    }

    getOrganizerUserInfo(userId: string): Promise<any> {
        return this.http.get('api/Users/OrganizerUser/' + userId)
            .toPromise()
            .then((response: Response) => response.json());
    }

    editAccount(account: any): Promise<any>{
        let headers = new Headers({ 'Content-Type': 'application/json' });
        headers.append('Authorization', 'Bearer ' + sessionStorage.getItem('accessToken'));
        let options = new RequestOptions({ headers: headers });

        return this.http.put('api/Users/EditUser/' + account.userId, account, options).toPromise();
    }

}