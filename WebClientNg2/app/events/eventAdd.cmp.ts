﻿import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import { EventService, Evnt, Category } from '../services/event.srv';
import { EventValidation } from '../helpers/validations/eventValidation';

import * as $ from 'jquery';

declare var $: any;

@Component({
    selector: 'event-add',
    templateUrl: 'app/events/eventAdd.view.html',
    styleUrls: ['app/events/eventAdd.css']
})

export class EventAddComponent implements OnInit {

    evnt: any;
    categories: Category[];
    priorities = ['Eriline', 'Tähtis', 'Tavaline'];
    errorMessage: string = "";

    constructor(private router: Router, private eventService: EventService) {
        this.evnt = {
            'Name': '',
            'CategoryIds': '',
            'Priority': '',
            'Location': '',
            'VolunteersLimit': '',
            'StartTime': '',
            'EndTime': '',
            'Description': '',
            'ImageUrl': ''
        }
    }

    addEvent() {
        this.evnt.CategoryIds = this.getSelectedCategories();
        var validation = new EventValidation(this.evnt);
        if (!validation.isValid) {
            $('#div-event-add-error').css("display", "block");
            this.errorMessage = validation.message;

        } else {
            this.eventService.addEvent(this.evnt, sessionStorage.getItem('accessToken'))
                .then(response => this.router.navigateByUrl('/events/' + response['eventId']))
                .catch(error => console.log(error));
        }
    }

    getSelectedCategories() {
        return this.categories
            .filter(item => item.selected === true)
            .map(item => item.categoryId);
    }

    select(item) {
        item.selected ? item.selected = false : item.selected = true;
    }


    getCategories() {
        this.eventService.getAllCategories().then(data => this.categories = data);
    }

    ngOnInit(): void {
        this.getCategories();
        if (!sessionStorage.getItem('accessToken')) {
            this.router.navigateByUrl('/login');
            console.log("Create event denied: User not logged in.")
            return;
        }
    }
}