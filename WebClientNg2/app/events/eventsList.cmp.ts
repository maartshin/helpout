﻿import { Component, OnInit } from '@angular/core';
import { EventService, Evnt, Category } from '../services/event.srv';
import { UpComingEventsComponent } from './upComingEvents.cmp';


@Component({
    selector: 'events-list',
    templateUrl: 'app/events/eventsList.view.html',
    styleUrls: ['app/events/eventsList.css'],
    
})

export class EventsListComponent implements OnInit {

    events: Evnt[] = [];
    categories: Category[]=[];
    selectedCategoryNames: String[];
    pagination: any[] = [];
    searchName: string = "";
    elementsOnPage: number = 4
    count: number;


    constructor(private eventService: EventService) { }

    getCategories() {
        this.eventService.getAllCategories().then(data => this.categories = data);
    }

    filterByChecked() {
        let selectedCategories = this.getSelectedCategories();
        this.selectedCategoryNames = selectedCategories;

        this.getEvents(1);

        this.getPagination();
    }
   

    getSelectedCategories() {
        return this.categories
            .filter(item => item.selected === true)
            .map(item => item.name);
    }

    select(item) {
        item.selected ? item.selected = false : item.selected = true;
        this.filterByChecked();
    }

    setEventClass(item: Evnt) {
        let priority = item.priority;
        let clss = "";
        switch (priority) {
            case 0:
                clss="special";
                break;
            case 1:
                clss = "important";
                break;
            default:
                clss = "basic";
                break;
        }
        return clss;
    }

    ngOnInit(): void {
        this.getCategories();
        this.getEvents(1);
    }

    getPagination(): void {
        this.pagination = [];
        let pages = Math.ceil(this.count / this.elementsOnPage);
        for (let i = 1; i <= pages; i++) {
            this.pagination.push(i);
        }
    }

    getEvents(page: number): void {
        let categories = this.categories
            .filter(item => item.selected === true)
            .map(item => item.categoryId);

        var filterData = {
            categories: categories,
            searchString: this.searchName,
            elementsOnPage: this.elementsOnPage,
            pageNumber: page
        }
        this.eventService.getPageEvents(filterData).then(result => {
            this.count = result.total;
            this.getPagination();
            this.events = result.events;
        });
    }
}