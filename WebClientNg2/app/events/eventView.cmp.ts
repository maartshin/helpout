﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';


import { EventService, Evnt, Comment } from '../services/event.srv';
import { AccountService } from '../services/account.srv';
import * as $ from 'jquery';

declare var $: any;

@Component({
    selector: 'event-view',
    templateUrl: 'app/events/eventView.view.html',
    styleUrls: ['app/events/eventView.css']
})

export class EventViewComponent implements OnInit {

    private eventId: number;
    evnt: Evnt;
    comments: Comment[];
    commentText: string;
    commentCount: number;
    participating: boolean;
    participantCount: number;
    participants: any;
    joinButtonText: string = "Liitu üritusega";
    allEvents: Evnt[] = [];
    similarEvents: Evnt[];
    organizer: any;
    isLoggedIn: boolean;
    volunteersLimit: any;

    constructor(private route: ActivatedRoute,
        private router: Router,
        private eventService: EventService,
        private accountService: AccountService) { }

    ngOnInit(): void {
        this.isLoggedIn = this.accountService.isLoggedIn();
        this.eventId = parseInt(this.route.snapshot.paramMap.get('id'));
        this.eventService.getEvent(this.eventId)
            .then(evnt => {
                this.evnt = evnt;
                this.volunteersLimit = evnt.volunteersLimit;
            })
            .then(() => this.getOrganizerInfo())
            .then(() => this.getEventParticipants())
            .then(() => this.setDateFormat());
        this.eventService.getComments(this.eventId).then(comments => {
            this.comments = comments.reverse();
            this.refreshComments();
            this.getParticipantCount();
            if (this.isLoggedIn) {
                this.isParticipant();
            }
        });
        this.getAllEvents().then(() => this.getSimilarEvents());
    }

    addComment(): void {
        let comment = new Comment(this.commentText);
        comment.eventId = this.eventId;
        console.log(this.evnt);
        this.eventService.addComment(this.eventId, comment)
            .then(() => this.eventService.getComments(this.eventId))
            .then(comments => {
                this.comments = comments.reverse();
                this.refreshComments();
                this.commentText = "";
            });
    }

    participate(): void {
        this.eventService.participate(this.eventId, sessionStorage.getItem('accessToken')).then((message) => {
            console.log(message);
            this.isParticipant();
            this.getParticipantCount();
        })
    }

    isParticipant() {
        this.eventService.isParticipant(this.eventId, sessionStorage.getItem('accessToken')).then(result => {
            this.participating = result;
            if (result) {
                this.joinButtonText = "Lahku ürituselt";
                return;
            }
            if (this.hasAvailablePlaces()) {
                this.joinButtonText = "Liitu üritusega";
                console.log(result);
            } else {
                this.joinButtonText = "";
            }
            
        });
    }

    refreshComments(): void {
        this.getTime();
        this.commentCount = this.comments.length;
    }

    getTime(): any {
        this.comments.map((item) => {
            let current = new Date().getTime();
            let posted = new Date(item.since).getTime();
            let difference = Math.round((current - posted) / 1000);
            item.since = this.getElapsedString(difference);

            return item;
        });
    }

    getElapsedString(time: number) : string {
        if (time < 3600) {
            return Math.round(time / 60).toString()+ " minutit tagasi";
        }
        else if (time < 86400) {
            return Math.round(time / 3600).toString() + " tundi tagasi";
        }
        else {
            return Math.round(time / 86400).toString() + " päeva tagasi";
        }
    }

    getParticipantCount(): void {
        this.eventService.participantCount(this.eventId).then((count) => {
            this.participantCount = count;
            console.log(this.participantCount);
        });
    }

    getAllEvents() {
        return this.eventService.getEvents()
            .then(events => this.allEvents = events);
    }

    getSimilarEvents() {
        setTimeout(() => {
            let eventCategories = this.evnt.categories.map(category => category.name);
            this.allEvents = this.allEvents.filter(event => event.eventId !== this.evnt.eventId);

            this.similarEvents = this.allEvents.filter(event => {
                let contains = false;
                for (let cat in eventCategories) {
                    for (let evntCat in event.categories) {
                        if (event.categories[evntCat].name == eventCategories[cat]) {
                            contains = true;
                            break;
                        }
                    }
                }
                return contains;
            });
        }, 500);
    }

    redirectToEvent(id: number) {
        this.router.navigateByUrl('/events/' + id).then(() => this.ngOnInit());
    }

    setDateFormat() {
        let startTime = new Date(this.evnt.startTime);
        let endTime = new Date(this.evnt.endTime);

        this.evnt.startTime = startTime.getFullYear()
            + "-" + ('0' + (startTime.getMonth() + 1)).slice(-2) + "-"
            + ('0' + startTime.getDate()).slice(-2) + " "
            + ('0' + startTime.getHours()).slice(-2) + ":"
            + ('0' + startTime.getMinutes()).slice(-2);

        this.evnt.endTime = endTime.getFullYear()
            + "-" + ('0' + (endTime.getMonth() + 1)).slice(-2) + "-"
            + ('0' + endTime.getDate()).slice(-2) + " "
            + ('0' + endTime.getHours()).slice(-2) + ":"
            + ('0' + endTime.getMinutes()).slice(-2);
    }

    getOrganizerInfo() {
        this.accountService.getOrganizerUserInfo(this.evnt.organizer.aspNetUserId)
            .then(organizer => this.organizer = organizer);
    }

    getEventParticipants() {
        this.eventService.getEventParticipants(this.eventId)
            .then(participants => this.participants = participants);
    }

    hasAvailablePlaces() {
        return this.evnt.participants.length < this.evnt.volunteersLimit;
    }
}