﻿import { Component, OnInit } from '@angular/core';

import { EventService, Evnt, Category } from '../services/event.srv';
import * as $ from 'jquery';

declare var $: any;


@Component({
    selector: 'upcoming-events',
    templateUrl: 'app/events/upComingEvents.view.html',
    styleUrls: ['app/events/upComingEvents.css']
})

export class UpComingEventsComponent implements OnInit {

    allUserEvents: Evnt[] = [];
    comingEvents: Evnt[] = [];
    pastEvents: Evnt[] = [];

    constructor(private eventService: EventService) { }

    ngOnInit(): void {
        this.eventService.getUpComingEvents(sessionStorage.getItem('accessToken'))
            .then(events => this.allUserEvents = events)
            .then(() => this.setComingEvents())
            .then(() => this.setPastEvents())
            .then(() => this.setDateFormatForAll());

        this.initJqueryEvents();
    }



    setComingEvents() {
        this.comingEvents = this.allUserEvents.filter(event => {
            var start = new Date(event.startTime).getTime()
            return start > Date.now();
        })
    }

    setPastEvents() {
        this.pastEvents = this.allUserEvents.filter(event => {
            var start = new Date(event.startTime).getTime()
            return start < Date.now();
        })
    }

    setDateFormatForAll() {
        this.allUserEvents.map(event => this.setDateFormat(event));
    }



    setDateFormat(evnt) {
        let startTime = new Date(evnt.startTime);
        let endTime = new Date(evnt.endTime);

        evnt.startTime = startTime.getFullYear()
            + "-" + ('0' + (startTime.getMonth() + 1)).slice(-2) + "-"
            + ('0' + startTime.getDate()).slice(-2) + " "
            + ('0' + startTime.getHours()).slice(-2) + ":"
            + ('0' + startTime.getMinutes()).slice(-2);

        evnt.endTime = endTime.getFullYear()
            + "-" + ('0' + (endTime.getMonth() + 1)).slice(-2) + "-"
            + ('0' + endTime.getDate()).slice(-2) + " "
            + ('0' + endTime.getHours()).slice(-2) + ":"
            + ('0' + endTime.getMinutes()).slice(-2);
    }

    initJqueryEvents() {
        $('button.pastEventButton').click(() => {
            let display = $('.pastEventTable').css('display');
            if (display === 'none') {
                $('.pastEventTable').css('display', 'block');
                $('.comingEventTable').css('display', 'none');
                $('.userData').css('display', 'none');
            }
            else {
                $('.pastEventTable').css('display', 'none');
            }
        });

        $('button.comingEventButton').click(() => {
            let display = $('.comingEventTable').css('display');
            if (display === 'none') {
                $('.pastEventTable').css('display', 'none');
                $('.comingEventTable').css('display', 'block');
                $('.userData').css('display', 'none');
            }
            else {
                $('.comingEventTable').css('display', 'none');
            }
        });

    }
}